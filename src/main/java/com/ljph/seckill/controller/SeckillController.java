package com.ljph.seckill.controller;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.core.ActionKey;
import com.jfinal.ext.interceptor.Restful;
import com.ljph.seckill.common.BaseController;
import com.ljph.seckill.dto.Exposer;
import com.ljph.seckill.dto.SeckillExecution;
import com.ljph.seckill.dto.SeckillResult;
import com.ljph.seckill.enums.SeckillState;
import com.ljph.seckill.exception.RepeatKillException;
import com.ljph.seckill.exception.SeckillCloseException;
import com.ljph.seckill.model.Seckill;
import com.ljph.seckill.service.SeckillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * Created by yuzhou on 16/8/30.
 */
@Before({Restful.class})
public class SeckillController extends BaseController {

    private static final Logger _log = LoggerFactory.getLogger(SeckillController.class);
    private SeckillService seckillService = this.enhance(SeckillService.class);
    /**
     * 秒杀列表页
     */
    public void index() {

        List<Seckill> list = seckillService.getSeckillList();
        SeckillResult<List> result = new SeckillResult<List>(true, list);
        renderJson(result);
    }

    /**
     * 秒杀详情页
     */
    public void show() {
        Long seckillId = getParaToLong();

        if(seckillId == null) {
            renderError(404);
            return;
        }

        Seckill seckill = seckillService.getById(seckillId);

        if(seckill == null) {
            renderError(404);
            return;
        }

        SeckillResult<Seckill> result = new SeckillResult<Seckill>(true, seckill);
        renderJson(result);
    }

    /**
     * 获取秒杀接口地址
     */
    @Clear({Restful.class})
    public void exposer() {
        SeckillResult<Exposer> result = null;
        try {
            Long seckillId = getParaToLong();
            Exposer exposer = seckillService.exportSeckillUrl(seckillId);
            result = new SeckillResult<Exposer>(exposer);
        } catch (Exception e) {
            _log.error(e.getMessage(), e);
            result = new SeckillResult<Exposer>("后台异常: " + e.getMessage());
        }

        renderJson(result);
    }

    /**
     * 执行秒杀 /execute/seckillId-md5
     */
    @Clear({Restful.class})
    public void execute() {
        SeckillResult<SeckillExecution> result = null;

        // 如下验证可以放在Validator或者拦截器Interceptor中处理
        Long phone = getCookieToLong("killPhone");
        if(phone == null) {
            result = new SeckillResult<SeckillExecution>("用户未注册");
            renderJson(result);
            return;
        }

        Long seckillId = getParaToLong(0);
        String md5 = getPara(1);

        try {
            SeckillExecution execution = seckillService.executeSeckill(seckillId, phone, md5);
            result = new SeckillResult<SeckillExecution>(execution);
        } catch (RepeatKillException e) {
            SeckillExecution execution = new SeckillExecution(seckillId, SeckillState.REPEAT_KILL);
            result = new SeckillResult<SeckillExecution>(true, execution);
        } catch (SeckillCloseException e) {
            SeckillExecution execution = new SeckillExecution(seckillId, SeckillState.END);
            result = new SeckillResult<SeckillExecution>(true, execution);
        } catch (Exception e) {
            _log.error("Exception when execute seckill: ", e);
            SeckillExecution execution = new SeckillExecution(seckillId, SeckillState.INNER_ERROR);
            result = new SeckillResult<SeckillExecution>(true, execution);
        }

        renderJson(result);
    }

    /**
     * 当前系统时间
     */
    @Clear({Restful.class})
    @ActionKey("/time/now")
    public void time() {
        Date now = new Date();
        renderJson(new SeckillResult<Date>(now));
    }
}
